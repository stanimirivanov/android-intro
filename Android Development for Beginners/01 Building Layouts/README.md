###### Image View - Quiz
[Link to the XML Visualizer](http://labs.udacity.com/android-visualizer/#/android/simple-imageview)

Local copy: [sampleImageView.xml](sampleImageView.xml)

- Change the height and width to be a fixed dp value (i.e. 100dp)
- change the scaleType between _center_ and _centerCrop_
- change to a different image (see _Available Images_ link)

###### Documentation - Quiz
[Link to the XML Visualizer](http://labs.udacity.com/android-visualizer/#/android/other-text-view-attributes)

Local copy: [documentation.xml](documentation.xml)

- find the XML attribute that makes _TextView_ __bold__ or _italic_
- use that XML attribute

###### LinearLayout - Quiz
[Link to the XML Visualizer](http://labs.udacity.com/android-visualizer/#/android/linear-layout)

Local copy: [linearLayout.xml](linearLayout.xml)

- add more _TextView_s to the parent _LinearLayout_ ViewGroup
- change the _LinearLayout_ orientation attribute

###### Width and Height - Quiz
[Link to the XML Visualizer](http://labs.udacity.com/android-visualizer/#/android/match-parent)

Local copy: [widthHeight.xml](widthHeight.xml)

- try different width and height values for each _TextView_
    - fixed dp values
    - wrap_content
    - match_parent

######  Layout Weight - Quiz
[Link to the XML Visualizer](http://labs.udacity.com/android-visualizer/#/android/linear-layout-weight)

Local copy: [layoutWeight.xml](layoutWeight.xml)

Build this layout! 

![LinearLayout](linearLayout.png)

Start with the XML provided below.
1. Determine width of each view first
2. Determine height of each view
3. Make sure the image takes up any remaining height in parent _LinearLayout_

######  Relative to Other Views - Quiz
[Link to the XML Visualizer](http://labs.udacity.com/android-visualizer/#/android/relative-layout-view-ids)

Local copy: [relativeToOtherViews.xml](relativeToOtherViews.xml)

Modify the provided XML layout to activate this desired layout

![RelativeToOtherViews](relativeToOtherViews.png)

######  List Item with RelativeLayout - Quiz
[Link to the XML Visualizer](http://labs.udacity.com/android-visualizer/#/android/relative-layout-list-item)

Local copy: [listItemWithRelativeLayout.xml](listItemWithRelativeLayout.xml)

Modify the provided XML to build this

![ListItemWithRelativeLayout](listItemWithRelativeLayout.jpg)

Hint: switch from _LinearLayout_ to _RelativeLayout_